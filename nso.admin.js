/**
 * @file
 * Enhancements for NSO node and node type edit forms.
 */

(function ($) {
  'use strict';

  /**
   * Update the summary for the module's vertical tab on the node edit form.
   */
  Drupal.behaviors.nsoNodeEditFieldsetSummaries = {
    attach: function (context) {
      $('fieldset#edit-group_nso', context).drupalSetSummary(function (context) {
        if ($('#edit-nso-enable-und', context).attr('checked')) {
          return Drupal.t('Enabled');
        }
        else {
          return Drupal.t('Disabled');
        }
      });
    }
  };

  /**
   * Update the summary for the module's vertical tab on the node type edit form.
   */
  Drupal.behaviors.nsoNodeTypeEditFieldsetSummaries = {
    attach: function (context) {
      $('fieldset#edit-nso', context).drupalSetSummary(function (context) {
        var vals = [];
        $('input[type=checkbox]', context).each(function () {
          if (this.checked && this.attributes['data-enabled-description']) {
            vals.push(this.attributes['data-enabled-description'].value);
          }
          else if (!this.checked && this.attributes['data-disabled-description']) {
            vals.push(this.attributes['data-disabled-description'].value);
          }
        });
        return vals.join(', ');
      });
    }
  };

})(jQuery);
